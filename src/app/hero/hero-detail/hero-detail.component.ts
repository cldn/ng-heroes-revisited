import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Hero, Weapon } from 'src/app/classes';
import { HeroService, WeaponService } from 'src/app/services';
import { MatDialog } from '@angular/material';
import { PickWeaponDialogComponent, RemoveWeaponDialogComponent, GenericDeleteDialogComponent } from 'src/app/dialogs';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {

  hero: Hero;
  weapons: Array<Weapon>;

  maxHeroPoints = 40;

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private weaponService: WeaponService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getHero();
    this.getWeapons();
  }

  getHero(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(
        hero => {
          this.hero = hero;
        });
  }

  public getWeapons(): void {
    this.weaponService.getWeapons()
      .subscribe(weapons => { this.weapons = weapons; });
  }

  public countRemainingPoints(rangePoints: number): number {
    const base = this.getRemainingPoints() + rangePoints;
    return base >= 40 ? 40 : base;
  }

  public getRemainingPoints(): number {
    return this.maxHeroPoints - this.hero.attackPts - this.hero.dodgePts - this.hero.damagePts - this.hero.healthPts;
  }

  public isValid(): boolean {
    return this.hero.attackPts > 0 && this.hero.damagePts > 0 && this.hero.dodgePts > 0 && this.hero.healthPts > 0
      && ((this.hero.attackPts + this.hero.dodgePts + this.hero.damagePts + this.hero.healthPts) === 40)
      && this.hero.name.length > 0;
  }

  public findWeaponName(weaponId: string): string {
    return this.weapons.find((weapon) => weapon.id === weaponId).name;
  }

  public pickWeapon(): void {
    // Open a "Pick a weapon" 400px dialog and give it the list of existing weapon items
    const dialogRef = this.dialog.open(PickWeaponDialogComponent, {
      width: '400px',
      data: { weapons: this.weapons }
    });

    dialogRef.afterClosed()
      .subscribe(
        (output) => {
          // If the dialog returned anything but undefined, it means we got a weapon ID, and it must be equipped
          if (output !== undefined) {
            this.weaponService.getWeapon(output)
              .subscribe(
                (weapon) => {
                  this.hero.weaponId = weapon.id;
                }
              );
          }
        }
      );
  }

  public removeWeapon(): void {
    // Open a "Remove a weapon" 400px dialog
    const dialogRef = this.dialog.open(RemoveWeaponDialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed()
      .subscribe(
        (output) => {
          // If the dialog returned true, it means the hero's weapon must be unequiped
          if (output) {
            this.hero.weaponId = null;
          }
        }
      );
  }

  public deleteHero(): void {
    // Open a generic delete 400px dialog to check if the hero must be deleted
    const dialogRef = this.dialog.open(GenericDeleteDialogComponent, {
      width: '400px',
      data: {
        title: 'Delete '.concat(this.hero.name),
        question: 'Are you sure that you want to delete '.concat(this.hero.name, ' from existence ?')
      }
    });

    dialogRef.afterClosed()
      .subscribe(
        (output) => {
          // If the dialog returned true, it means the hero must be deleted
          if (output) {
            console.log('delete this hero');
          }
        }
      );
  }

  public resetStats(): void {
    this.hero.attackPts = this.hero.damagePts = this.hero.dodgePts = this.hero.healthPts = 0;
  }

  save(): void {
    this.heroService.updateHero(this.hero.id, this.hero);
  }

}
