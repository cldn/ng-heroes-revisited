import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Hero } from 'src/app/classes';
import { HeroService, WeaponService } from 'src/app/services';
import { Weapon } from '../../../classes/weapon';

@Component({
  selector: 'app-add-hero',
  templateUrl: './add-hero.component.html',
  styleUrls: ['./add-hero.component.scss']
})
export class AddHeroComponent implements OnInit {

  heroForm: FormGroup;
  existingWeapons: Array<Weapon>;

  maxHeroPoints = 40;

  constructor(
    private heroService: HeroService,
    private weaponService: WeaponService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.heroForm = this.fb.group({
      heroName: ['', [Validators.required, Validators.minLength(3)]],
      heroWeapon: [null],
      heroAtq: [0, [Validators.required, Validators.min(1)]],
      heroDod: [0, [Validators.required, Validators.min(1)]],
      heroDmg: [0, [Validators.required, Validators.min(1)]],
      heroHp: [0, [Validators.required, Validators.min(1)]]
    });
  }

  ngOnInit() {
    this.weaponService.getWeapons()
      .subscribe((weapons) => this.existingWeapons = weapons);
  }

  public countRemainingPoints(rangePoints: number): number {
    const base = this.getRemainingPoints() + rangePoints;
    return base >= 40 ? 40 : base;
  }

  public getRemainingPoints(): number {
    return this.maxHeroPoints - this.heroAtq.value - this.heroDod.value - this.heroDmg.value - this.heroHp.value;
  }

  public resetRanges(): void {
    this.heroAtq.setValue(0);
    this.heroDod.setValue(0);
    this.heroDmg.setValue(0);
    this.heroHp.setValue(0);
  }

  public createHero(): void {
    const newHero = new Hero();
    newHero.name = this.heroName.value;
    newHero.weaponId = this.heroWeaponId.value;
    newHero.attackPts = this.heroAtq.value;
    newHero.dodgePts = this.heroDod.value;
    newHero.damagePts = this.heroDmg.value;
    newHero.healthPts = this.heroHp.value;

    this.heroService.addHero(newHero)
      .then(
        (value) => {
          this.router.navigate(['/heroes/detail', value.id]);
        }
      );
  }

  get heroName(): FormControl {
    return this.heroForm.get('heroName') as FormControl;
  }

  get heroWeaponId(): FormControl {
    return this.heroForm.get('heroWeapon') as FormControl;
  }

  get heroAtq(): FormControl {
    return this.heroForm.get('heroAtq') as FormControl;
  }

  get heroDod(): FormControl {
    return this.heroForm.get('heroDod') as FormControl;
  }

  get heroDmg(): FormControl {
    return this.heroForm.get('heroDmg') as FormControl;
  }

  get heroHp(): FormControl {
    return this.heroForm.get('heroHp') as FormControl;
  }

}
