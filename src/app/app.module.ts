import { LayoutModule } from '@angular/cdk/layout';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BattleComponent } from './battle';
import { DashboardComponent } from './dashboard';
import { BattleDialogComponent, GenericDeleteDialogComponent, PickWeaponDialogComponent, RemoveWeaponDialogComponent } from './dialogs';
import { AddHeroComponent, HeroDetailComponent, HeroesComponent, HeroSearchComponent } from './hero';
import { InformationsComponent } from './informations';
import { MaterialModule } from './material.module';
import { MessagesComponent } from './messages';
import { NavigationComponent } from './navigation';
import { WeaponAddComponent, WeaponDetailComponent, WeaponsComponent, WeaponSearchComponent } from './weapon';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase, environment.firebase.projectId),
    AngularFirestoreModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule
  ],
  declarations: [
    AppComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    DashboardComponent,
    HeroSearchComponent,
    NavigationComponent,
    AddHeroComponent,
    InformationsComponent,
    BattleComponent,
    // Weapon component declarations
    WeaponAddComponent,
    WeaponDetailComponent,
    WeaponSearchComponent,
    WeaponsComponent,
    // Dialog components
    BattleDialogComponent,
    GenericDeleteDialogComponent,
    PickWeaponDialogComponent,
    RemoveWeaponDialogComponent
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    BattleDialogComponent,
    GenericDeleteDialogComponent,
    PickWeaponDialogComponent,
    RemoveWeaponDialogComponent
  ]
})
export class AppModule { }
