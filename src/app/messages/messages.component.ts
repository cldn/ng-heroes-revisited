import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/services';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor(public messageService: MessageService) { }

  public messages: Array<String>;

  ngOnInit() {
    this.messages = this.messageService.messages;
  }

  public clear(): void {
    this.messageService.clear();
  }

}
