import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { WeaponService } from 'src/app/services';
import { Hero } from '../../classes/hero';
import { Weapon } from '../../classes/weapon';

@Component({
  selector: 'app-battle-dialog',
  templateUrl: './battle-dialog.component.html',
  styleUrls: ['./battle-dialog.component.scss']
})
export class BattleDialogComponent implements OnInit {

  private player1: Hero;
  private player2: Hero;
  private weapon1: Weapon;
  private weapon2: Weapon;
  private logs: Array<String> = new Array<String>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private weaponService: WeaponService
  ) {
    this.player1 = data.player1;
    this.player2 = data.player2;
  }

  ngOnInit() {
    this.pullFirstWeapon();
  }

  private pullFirstWeapon(): void {
    if (this.player1.weaponId) {
      this.weaponService.getWeapon(this.player1.weaponId)
        .subscribe(
          (weapon) => {
            this.weapon1 = weapon;
            this.pullSecondWeapon();
          },
          (error) => {
            console.error(error);
          }
        );
    } else {
      this.pullSecondWeapon();
    }
  }

  private pullSecondWeapon(): void {
    if (this.player2.weaponId) {
      this.weaponService.getWeapon(this.player2.weaponId)
        .subscribe(
          (weapon) => {
            this.weapon2 = weapon;
            this.fight();
          },
          (error) => {
            console.error(error);
          }
        );
    } else {
      this.fight();
    }
  }

  private fight(): void {
    /*
    Calculate each player's hitscore.
    The hitscore is calculated on the attack points of the attacker and its weapon, divided by the defender dodge stats.
    A hitscore is a percentage between 0 and 100.
    e.g with a hitscore of 65, an attacker will successfully hit the defender if he rolls the dice between 0 and 65
    */
    const player1HitScore = ((this.player1.attackPts + (this.weapon1 ? this.weapon1.attackPts : 0))
      / (this.player2.dodgePts + (this.weapon2 ? this.weapon2.dodgePts : 0))) * 50;
    const player2HitScore = ((this.player2.attackPts + (this.weapon2 ? this.weapon2.attackPts : 0))
      / (this.player1.dodgePts + (this.weapon1 ? this.weapon1.dodgePts : 0))) * 50;
    let winner: Hero;

    // Log the beginning of the fight and the hitscore of both players
    this.log(`${this.player1.name} and ${this.player2.name} start a fight ! ${this.player1.name} attacks first..`);
    this.log(`${this.player1.name} has a hitscore of ${player1HitScore}%.`);
    this.log(`${this.player2.name} has a hitscore of ${player2HitScore}%.`);

    while (!winner) {
      // Check the player is alive before he plays his turn
      if (this.player1.isAlive()) {
        // Roll a dice between 0 and 100 to determine the roll
        const player1Roll = Math.random() * 100;
        // Start the attack
        this.attack(this.player1, player1Roll, this.weapon1, this.player2);
      } else {
        // Player 1 is dead, the winner is his enemy
        winner = this.player2;
        break;
      }

      // Do the same thing for player 2
      if (this.player2.isAlive()) {
        const player2Roll = Math.random() * 100;
        this.attack(this.player2, player2Roll, this.weapon2, this.player1);
      } else {
        winner = this.player1;
        break;
      }
    }
    // A hero died, we can end the fight and log the final results
    this.log(`Fight has ended.`);
    this.log(`And the winner is.. ${winner.name.toUpperCase()} !!!`);
  }

  private attack(attacker: Hero, attackerHitScore: number, attackerWeapon: Weapon, defender: Hero): void {
    const attackerRoll = Math.random() * 100;
    // If the roll is between 0 and the hitscore
    if (attackerRoll <= attackerHitScore) {
      // The attacker can hit, calculate the hit damage
      const damageDealt = (attacker.damagePts + (attackerWeapon ? attackerWeapon.damagePts : 0)) / 2;
      // Lower the defender's health points
      defender.healthPts -= damageDealt;
      // Log the hit
      this.log(`${attacker.name} hits ${defender.name} for ${damageDealt} damage !!`);
    } else {
      // The defender dodged, log the fail
      this.log(`${attacker.name} tried to hit ${defender.name}, but miserably failed..`);
    }
  }

  private log(msg: string): void {
    this.logs.push(msg);
  }

}
