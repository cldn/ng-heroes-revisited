import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveWeaponDialogComponent } from './remove-weapon-dialog.component';

describe('RemoveWeaponDialogComponent', () => {
  let component: RemoveWeaponDialogComponent;
  let fixture: ComponentFixture<RemoveWeaponDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveWeaponDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveWeaponDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
