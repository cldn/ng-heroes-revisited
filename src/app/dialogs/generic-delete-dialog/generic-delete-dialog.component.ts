import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-generic-delete-dialog',
  templateUrl: './generic-delete-dialog.component.html',
  styleUrls: ['./generic-delete-dialog.component.scss']
})
export class GenericDeleteDialogComponent {

  public title: string;
  public question: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.title = this.data['title'];
    this.question = this.data['question'];
  }

}
