import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickWeaponDialogComponent } from './pick-weapon-dialog.component';

describe('PickWeaponDialogComponent', () => {
  let component: PickWeaponDialogComponent;
  let fixture: ComponentFixture<PickWeaponDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickWeaponDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickWeaponDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
