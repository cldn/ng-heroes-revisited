import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';
import { Weapon } from 'src/app/classes';

@Component({
  selector: 'app-pick-weapon-dialog',
  templateUrl: './pick-weapon-dialog.component.html',
  styleUrls: ['./pick-weapon-dialog.component.scss']
})
export class PickWeaponDialogComponent {

  heroWeaponForm: FormGroup;
  existingWeapons: Array<Weapon>;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder
  ) {
    this.existingWeapons = this.data['weapons'];
    this.heroWeaponForm = this.fb.group({
      'weaponChoice': ['', Validators.required]
    });
  }

}
