import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Weapon } from 'src/app/classes';
import { WeaponService } from 'src/app/services';

@Component({
  selector: 'app-weapon-add',
  templateUrl: './weapon-add.component.html',
  styleUrls: ['./weapon-add.component.scss']
})
export class WeaponAddComponent {

  weaponForm: FormGroup;

  constructor(
    private weaponService: WeaponService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.weaponForm = this.fb.group({
      weaponName: ['', [Validators.required, Validators.minLength(3)]],
      weaponAtq: [0, [Validators.required, Validators.min(-5), Validators.max(5)]],
      weaponDod: [0, [Validators.required, Validators.min(-5), Validators.max(5)]],
      weaponDmg: [0, [Validators.required, Validators.min(-5), Validators.max(5)]],
      weaponHp: [0, [Validators.required, Validators.min(-5), Validators.max(5)]]
    });
  }

  public statsSum(): number {
    return this.weaponAtq.value + this.weaponDod.value + this.weaponDmg.value + this.weaponHp.value;
  }

  public resetRanges(): void {
    this.weaponAtq.setValue(0);
    this.weaponDod.setValue(0);
    this.weaponDmg.setValue(0);
    this.weaponHp.setValue(0);
  }

  public createWeapon(): void {
    const newWeapon = new Weapon();
    newWeapon.name = this.weaponName.value;
    newWeapon.attackPts = this.weaponAtq.value;
    newWeapon.dodgePts = this.weaponDod.value;
    newWeapon.damagePts = this.weaponDmg.value;
    newWeapon.healthPts = this.weaponHp.value;

    this.weaponService.addWeapon(newWeapon)
      .then(
        (value) => {
          this.router.navigate(['/weapons/detail', value.id]);
        }
      );
  }

  get weaponName(): FormControl {
    return this.weaponForm.get('weaponName') as FormControl;
  }

  get weaponAtq(): FormControl {
    return this.weaponForm.get('weaponAtq') as FormControl;
  }

  get weaponDod(): FormControl {
    return this.weaponForm.get('weaponDod') as FormControl;
  }

  get weaponDmg(): FormControl {
    return this.weaponForm.get('weaponDmg') as FormControl;
  }

  get weaponHp(): FormControl {
    return this.weaponForm.get('weaponHp') as FormControl;
  }

}
