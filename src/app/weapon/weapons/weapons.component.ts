import { Component, OnInit } from '@angular/core';
import { Weapon } from 'src/app/classes';
import { WeaponService } from 'src/app/services';

@Component({
  selector: 'app-weapons',
  templateUrl: './weapons.component.html',
  styleUrls: ['./weapons.component.scss']
})
export class WeaponsComponent implements OnInit {

  weapons: Weapon[];
  query = '';

  constructor(private weaponService: WeaponService) { }

  ngOnInit() {
    this.getWeapons();
  }

  public getWeapons(): void {
    this.weaponService.getWeapons()
      .subscribe(weapons => this.weapons = weapons);
  }

  // public add(name: string): void {
  //   name = name.trim();
  //   if (!name) { return; }
  //   this.weaponService.addWeapon({ name } as Weapon)
  //     .subscribe(weapon => {
  //       this.weapons.push(weapon);
  //     });
  // }

  // public delete(weapon: Weapon): void {
  //   this.weapons = this.weapons.filter(w => w !== weapon);
  //   this.weaponService.deleteWeapon(weapon).subscribe();
  // }

}
