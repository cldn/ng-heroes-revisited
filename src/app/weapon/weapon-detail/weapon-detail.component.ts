import { Component, OnInit } from '@angular/core';
import { Weapon } from 'src/app/classes';
import { ActivatedRoute } from '@angular/router';
import { WeaponService } from 'src/app/services';

@Component({
  selector: 'app-weapon-detail',
  templateUrl: './weapon-detail.component.html',
  styleUrls: ['./weapon-detail.component.scss']
})
export class WeaponDetailComponent implements OnInit {

  weapon: Weapon;

  weaponBalancePoints = 0;

  constructor(
    private route: ActivatedRoute,
    private weaponService: WeaponService
  ) { }

  ngOnInit(): void {
    this.getWeapon();
  }

  getWeapon(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.weaponService.getWeapon(id)
      .subscribe(
        weapon => {
          this.weapon = weapon;
        });
  }

  public statsSum(): number {
    return this.weapon.attackPts + this.weapon.dodgePts + this.weapon.damagePts + this.weapon.healthPts;
  }

  public isValid(): boolean {
    return this.statsSum() === this.weaponBalancePoints && this.weapon.name.length > 0;
  }

  public resetStats(): void {
    this.weapon.attackPts = this.weapon.damagePts = this.weapon.dodgePts = this.weapon.healthPts = 0;
  }

  save(): void {
    this.weaponService.updateWeapon(this.weapon.id, this.weapon);
  }

}
