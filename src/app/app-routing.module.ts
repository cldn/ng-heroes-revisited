import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BattleComponent } from './battle';
import { DashboardComponent } from './dashboard';
import { AddHeroComponent, HeroDetailComponent, HeroesComponent } from './hero';
import { InformationsComponent } from './informations';
import { WeaponAddComponent, WeaponDetailComponent, WeaponsComponent } from './weapon';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'heroes/detail/:id', component: HeroDetailComponent },
  {
    path: 'heroes',
    component: HeroesComponent,
    children: [
      { path: 'add', component: AddHeroComponent }
    ]
  },
  { path: 'weapons/detail/:id', component: WeaponDetailComponent },
  {
    path: 'weapons',
    component: WeaponsComponent,
    children: [
      { path: 'add', component: WeaponAddComponent }
    ]
  },
  { path: 'battle', component: BattleComponent },
  { path: 'informations', component: InformationsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
