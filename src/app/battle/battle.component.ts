import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Hero } from 'src/app/classes';
import { HeroService } from 'src/app/services';
import { BattleDialogComponent } from '../dialogs';

@Component({
  selector: 'app-battle',
  templateUrl: './battle.component.html',
  styleUrls: ['./battle.component.scss']
})
export class BattleComponent implements OnInit {

  public availableHeroes: Array<Hero>;
  public battleForm: FormGroup;

  constructor(
    private heroService: HeroService,
    private fb: FormBuilder,
    private dialog: MatDialog
  ) {
    this.battleForm = this.fb.group({
      player1: ['', Validators.required],
      player2: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.heroService.getHeroes()
      .subscribe(heroes => this.availableHeroes = heroes);
  }

  public fight(): void {
    // Open the battle dialog and give it clones of the players, so that their real stats are not affected by the fight
    // This way, you can do the fight several times with idempotent results and health points not being modified by the fight
    this.dialog.open(BattleDialogComponent, {
      data: {
        player1: this.clone(this.player1),
        player2: this.clone(this.player2)
      }
    });
  }

  private clone(hero: Hero): Hero {
    const clone = new Hero();
    clone.id = hero.id;
    clone.name = hero.name;
    clone.attackPts = hero.attackPts;
    clone.dodgePts = hero.dodgePts;
    clone.damagePts = hero.damagePts;
    clone.healthPts = hero.healthPts;
    clone.weaponId = hero.weaponId;
    return clone;
  }

  get player1(): Hero {
    return this.battleForm.get('player1').value;
  }

  get player2(): Hero {
    return this.battleForm.get('player2').value;
  }

}
