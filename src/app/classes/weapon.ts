import { Serializable } from './serializable';

export class Weapon extends Serializable {

    public id: string;
    public name: string;
    public attackPts: number;
    public dodgePts: number;
    public damagePts: number;
    public healthPts: number;

}
