import { Serializable } from './serializable';

export class Hero extends Serializable {

    public id: string;
    public name: string;
    public attackPts: number;
    public dodgePts: number;
    public damagePts: number;
    public healthPts: number;
    public weaponId: string;

    public isAlive(): boolean {
        return this.healthPts > 0;
    }

}
