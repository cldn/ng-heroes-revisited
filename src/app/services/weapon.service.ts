import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Weapon } from '../classes';
import { MessageService } from './message.service';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class WeaponService {

  private static collectionName = 'weapons';

  constructor(
    private messageService: MessageService,
    private db: AngularFirestore
  ) { }

  public getWeapons(): Observable<Array<Weapon>> {
    return this.db.collection<Weapon>(WeaponService.collectionName)
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const weapon = new Weapon().fromJSON(data);
            const id = a.payload.doc.id;
            weapon.id = id;
            return weapon;
          });
        })
      );
  }

  private getWeaponDocument(id: string): AngularFirestoreDocument<Weapon> {
    return this.db.doc<Weapon>(WeaponService.collectionName + '/' + id);
  }

  public getWeapon(id: string): Observable<Weapon> {
    return this.getWeaponDocument(id).snapshotChanges()
      .pipe(
        map(a => {
          const data = a.payload.data() as Weapon;
          const weapon = new Weapon().fromJSON(data);
          weapon.id = id;
          return weapon;
        })
      );
  }

  public addWeapon(weapon: Weapon): Promise<firebase.firestore.DocumentReference> {
    return this.db.collection<Weapon>(WeaponService.collectionName).add(Object.assign({}, weapon));
  }

  public updateWeapon(id: string, weapon: Weapon) {
    this.getWeaponDocument(id).update(Object.assign({}, weapon));
  }

  public deleteWeapon(id: string) {
    this.getWeaponDocument(id).delete();
  }

  private log(message: string) {
    this.messageService.add(`WeaponService: ${message}`);
  }
}
