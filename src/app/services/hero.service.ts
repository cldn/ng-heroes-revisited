import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, CollectionReference } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Hero } from '../classes';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private static collectionName = 'heroes';

  constructor(
    private messageService: MessageService,
    private db: AngularFirestore
  ) { }

  public getHeroes(): Observable<Array<Hero>> {
    return this.db.collection<Hero>(HeroService.collectionName)
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const hero = new Hero().fromJSON(data);
            const id = a.payload.doc.id;
            hero.id = id;
            return hero;
          });
        })
      );
  }

  public getHeroesTop3(): Observable<Array<Hero>> {
    return this.db.collection(HeroService.collectionName, ref => ref.limit(3))
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const data = a.payload.doc.data();
            const hero = new Hero().fromJSON(data);
            const id = a.payload.doc.id;
            hero.id = id;
            return hero;
          });
        })
      );
  }

  private getHeroDocument(id: string): AngularFirestoreDocument<Hero> {
    return this.db.doc<Hero>(HeroService.collectionName + '/' + id);
  }

  public getHero(id: string): Observable<Hero> {
    return this.getHeroDocument(id).snapshotChanges()
      .pipe(
        map(a => {
          const data = a.payload.data() as Hero;
          const hero = new Hero().fromJSON(data);
          hero.id = id;
          return hero;
        })
      );
  }

  public addHero(hero: Hero): Promise<firebase.firestore.DocumentReference> {
    return this.db.collection<Hero>(HeroService.collectionName).add(Object.assign({}, hero));
  }

  public updateHero(id: string, hero: Hero) {
    this.getHeroDocument(id).update(Object.assign({}, hero));
  }

  public deleteHero(id: string) {
    this.getHeroDocument(id).delete();
  }

  /* GET heroes whose name contains search term */
  searchHeroes(term: string): Observable<Array<Hero>> {
    if (!term.trim()) {
      // if not search term, return empty hero array.
      return of([]);
    }

    // const query = this.db.collection<Hero>(HeroService.collectionName, ref => ref.where('name', '==', term))
    // .snapshotChanges()
    // .pipe(
    //   map(actions => {
    //     return actions.map(a => {
    //       const data = a.payload.doc.data();
    //       const hero = new Hero().fromJSON(data);
    //       const id = a.payload.doc.id;
    //       hero.id = id;
    //       return hero;
    //     });
    //   })
    // );
    // query.subscribe(
    //   value => {
    //     console.log(value);
    //   }
    // );
    return this.getHeroes().pipe(
      map(
        heroes => {
          const heroesResult = new Array<Hero>();
          heroes.forEach(hero => {
            if (hero.name.includes(term)) {
              heroesResult.push(hero);
            }
          });
          return heroesResult;
        }
      )
    );
  }


  private log(message: string) {
    this.messageService.add(`HeroService: ${message}`);
  }
}
